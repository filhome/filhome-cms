import { TOAST_COLOR } from '@/utils'
export const state = () => ({
  toast: {
    content: '',
    color: '',
    isReset: false,
  },
})
export const getters = {
  toast(state) {
    return state.toast
  },
  isReset(state) {
    return state.toast.isReset
  },
}
export const mutations = {
  SHOW_MESSAGE(state, payload) {
    state.toast.content = payload.content
    state.toast.color = payload.color
    state.toast.isReset = false
  },
  RESET_MESSAGE(state) {
    state.toast.content = ''
    state.toast.color = TOAST_COLOR.INFO
    state.toast.isReset = true
  },
}
export const actions = {
  showMessage({ commit }, payload) {
    commit('SHOW_MESSAGE', payload)
  },
  resetNotifier({ commit }) {
    commit('RESET_MESSAGE')
  },
}

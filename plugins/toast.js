import { TOAST_COLOR } from '@/utils'
export default ({ app, store }, inject) => {
  inject('toast', {
    show({ content = '', color = '' }) {
      store.dispatch('toast/showMessage', {
        content,
        color,
      })
    },

    success(content = '') {
      store.dispatch('toast/showMessage', {
        content,
        color: TOAST_COLOR.SUCCESS,
      })
    },

    error(content = '') {
      store.dispatch('toast/showMessage', {
        content,
        color: TOAST_COLOR.ERROR,
      })
    },

    warning(content = '') {
      store.dispatch('toast/showMessage', {
        content,
        color: TOAST_COLOR.WARNING,
      })
    },

    info(content = '') {
      store.dispatch('toast/showMessage', {
        content,
        color: TOAST_COLOR.INFO,
      })
    },
  })
}

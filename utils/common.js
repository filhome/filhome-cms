const RULES = {
  PASSWORD: /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[A-Za-z\d$@$!%*#?&]{8,}$/,
  EMAIL: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
  PHONE: /(84|0[3|5|7|8|9])+([0-9]{8,})\b/
}

const TOAST_COLOR = {
  SUCCESS: 'PRIMARY',
  ERROR: 'ERROR',
  WARNING: 'WARNING',
  INFO: 'INFO',
}

const POPUP_TYPE = {
  ALERT: 'ALERT',
  POPUP: 'POPUP',
}

export { RULES, TOAST_COLOR, POPUP_TYPE }

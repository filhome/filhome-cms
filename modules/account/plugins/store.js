import masterDataStore from '../modules/account/store/index'

export default async ({ store }) => {
  return await store.registerModule('account', masterDataStore, {
    namespaced: masterDataStore.namespaced
  })
}
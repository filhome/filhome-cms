const path = require('path')

module.exports = function registerModule(moduleOptions) {
  this.nuxt.hook('components:dirs', (dirs) => {
    dirs.push({
      path: path.resolve(__dirname, 'components')
    })
  })

  this.extendRoutes((routes) => {
    routes.unshift({
      name: 'account-profile',
      path: '/account/profile/',
      component: path.resolve(__dirname, 'pages/profile.vue')
    })
  })

  this.addPlugin(path.resolve(__dirname, 'plugins/store.js'))
}
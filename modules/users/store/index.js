export const namespaced = true

export const state = () => ({})

export const mutations = {}

export const actions = {}

export const getters = {}

export default {
  namespaced,
  state,
  mutations,
  actions,
  getters
}
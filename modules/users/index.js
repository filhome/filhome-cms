const path = require('path')

module.exports = function registerModule(moduleOptions) {
  this.nuxt.hook('components:dirs', (dirs) => {
    dirs.push({
      path: path.resolve(__dirname, 'components'),
    })
  })

  this.extendRoutes((routes) => {
    routes.unshift(
      {
        name: 'users-managerment',
        path: '/users-managerment/',
        component: path.resolve(__dirname, 'pages/index.vue'),
      },
      {
        name: 'users-managerment-customer',
        path: '/users-managerment/customer',
        component: path.resolve(__dirname, 'pages/customer.vue'),
      },
      {
        name: 'users-managerment-staff',
        path: '/users-managerment/staff',
        component: path.resolve(__dirname, 'pages/staff.vue'),
      }
    )
  })

  this.addPlugin(path.resolve(__dirname, 'plugins/store.js'))
}

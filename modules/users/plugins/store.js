import masterDataStore from '../modules/users/store/index'

export default async ({ store }) => {
  return await store.registerModule('users', masterDataStore, {
    namespaced: masterDataStore.namespaced
  })
}
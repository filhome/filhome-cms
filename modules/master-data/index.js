const path = require('path')

module.exports = function registerModule(moduleOptions) {
  this.nuxt.hook('components:dirs', (dirs) => {
    dirs.push({
      path: path.resolve(__dirname, 'components')
    })
  })

  this.extendRoutes((routes) => {
    routes.unshift({
      name: 'master-roles',
      path: '/master/roles/',
      component: path.resolve(__dirname, 'pages/roles.vue')
    })
  })

  this.addPlugin(path.resolve(__dirname, 'plugins/store.js'))
}
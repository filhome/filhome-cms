import masterDataStore from '../modules/master-data/store/index'

export default async ({ store }) => {
  return await store.registerModule('master', masterDataStore, {
    namespaced: masterDataStore.namespaced
  })
}
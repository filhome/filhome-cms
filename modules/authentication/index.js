const path = require('path')
module.exports = function registerModule(moduleOptions) {
  this.nuxt.hook('components:dirs', (dirs) => {
    dirs.push({
      path: path.resolve(__dirname, 'components')
    })
  })

  this.extendRoutes((routes) => {
    routes.unshift({
      name: 'auth-login',
      path: '/auth/login/',
      component: path.resolve(__dirname, 'pages/login.vue')
    }, {
      name: 'auth-register',
      path: '/auth/register/',
      component: path.resolve(__dirname, 'pages/register.vue')
    }, {
      name: 'auth-forgot-password',
      path: '/auth/forgot-password/',
      component: path.resolve(__dirname, 'pages/forgot-password.vue')
    }, {
      name: 'auth-change-password',
      path: '/auth/change-password/',
      component: path.resolve(__dirname, 'pages/change-password.vue')
    }, {
      name: 'auth-reset-password',
      path: '/auth/reset-password/',
      component: path.resolve(__dirname, 'pages/reset-password.vue')
    }, {
      name: 'auth-logout',
      path: '/auth/logout/',
      component: path.resolve(__dirname, 'pages/logout.vue')
    })
  })
}